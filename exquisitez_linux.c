#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

sem_t aviso,mutex,fuente;

void * cocineros(){

	while(1){
		printf("COCINERO durmiendo\n");
		sem_wait(&aviso);
		sem_wait(&mutex);
			printf("COCINERO cocinando\n");
			sem_post(&fuente);
		sem_post(&mutex);
	}
}

void * comensales(){

	while(1){
		sem_wait(&mutex);

		if(sem_trywait(&fuente)!=0){
			printf("COMENSAL La fuente esta vacia\n");
			sem_post(&aviso);
			sem_post(&mutex);
			sem_wait(&fuente);
			printf("COMENSAL Pusieron comida en la fuente\n");
		}
		else
			printf("COMENSAL Hay comida\n"); 
			sem_post(&mutex);
	}
}

int main(){

	pthread_t cocinero[2],comensal;
	int ret;
	sem_init(&aviso,0,0);
	sem_init(&mutex,0,1);
	sem_init(&fuente,0,0);

	ret = pthread_create( &cocinero[0], NULL, cocineros, NULL );
    
    if(ret){
      	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
    }

    ret = pthread_create( &cocinero[1], NULL, cocineros, NULL );
    
    if(ret){
      	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
    }

	ret = pthread_create( &comensal, NULL, comensales,NULL);
    if(ret){
        printf("Error - pthread_create() return code: %d\n",ret);
        exit(EXIT_FAILURE);
    }

    pthread_join(cocinero[0], NULL);
	pthread_join(cocinero[1], NULL);
	pthread_join(comensal, NULL);

    return 0;
}
