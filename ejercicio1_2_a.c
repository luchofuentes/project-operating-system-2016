#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define NTHREADS 5

sem_t semaforos[NTHREADS];

void * thread_cycle( void * letra ){
	printf("hola");

	while(1){
		sem_wait(&semaforos[((int) (*(char*)letra) - 'A') %5]);
		printf("%c", (*(char*)letra));
		sem_post(&semaforos[((int) ((*(char*)letra) - 'A')+1) %5]);
	}
}


int main(){

	pthread_t threads [NTHREADS];
	int i,j,ret;
	char letras [NTHREADS] = {'A','B','C','D','E'};
	
	for(int i=0; i<NTHREADS; i++){
		sem_init(&semaforos[i],0,0);
	}
	sem_post(&semaforos[0]);
	for(int i=0;i<NTHREADS;i++){

	 	ret = pthread_create( &threads[i], NULL, thread_cycle, (void*) &letras[i]);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
    }

	for(j=0; j < NTHREADS; j++){

	      pthread_join( threads[j], NULL);
	}
     exit(EXIT_SUCCESS);
}