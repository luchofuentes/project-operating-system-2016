#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define NTHREADS 5

void * thread_cycle( void * letra ){

	int i;
	char * c = (char *) letra;
	for(i=0;i<10000;i++){
		printf("%c",*c);
	}
}


int main(){

	pthread_t threads [NTHREADS];
	int i,j,ret;
	char letras [NTHREADS] = {'A','B','C','D','E'};

	for(int i=0;i<NTHREADS;i++){

	 	ret = pthread_create( &threads[i], NULL, thread_cycle, (void*) &letras[i]);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
    }

	for(j=0; j < NTHREADS; j++){

	      pthread_join( threads[j], NULL);
	}
	
     exit(EXIT_SUCCESS);
}