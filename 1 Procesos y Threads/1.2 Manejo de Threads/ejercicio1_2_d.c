#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define NTHREADS 5

sem_t semAB1,semAB2,sem1o2,semC,semD,semE;

void * thread_A(){
	while(1){
		sem_wait(&semAB1);
		printf("A");
		if(sem_trywait(&semAB2)==0){
			sem_post(&semAB1);
		}
		else{
			if(sem_trywait(&sem1o2)!=0){
				sem_post(&sem1o2);
				sem_post(&semAB2);
			}	
			sem_post(&semC);
		}
	}
	
}
void * thread_B(){
	while(1){
		sem_wait(&semAB1);
		printf("B");
		if(sem_trywait(&semAB2)==0){
			sem_post(&semAB1);
		}
		else{
			if(sem_trywait(&sem1o2)!=0){
				sem_post(&sem1o2);
				sem_post(&semAB2);
			}	
			sem_post(&semC);
		}
	}
}
void * thread_C(){
	while(1){
		sem_wait(&semC);
		printf("C");
		sem_post(&semD);
	}
	
}
void * thread_D(){
	while(1){
		sem_wait(&semD);
		printf("D");
		sem_post(&semE);
	}
	
}
void * thread_E(){
	while(1){
		sem_wait(&semE);
		printf("E");
		sem_post(&semAB1);
	}
	
}

int main(){

	pthread_t threads [NTHREADS];
	int j,ret;
	sem_init(&semAB1,0,1);
	sem_init(&semAB2,0,1);
	sem_init(&sem1o2,0,1);
	sem_init(&semC,0,0);
	sem_init(&semD,0,0);
	sem_init(&semE,0,0);
	
	//bloque de creacion de Threads
	ret = pthread_create( &threads[0], NULL, thread_A, NULL );
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &threads[1], NULL, thread_B,NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &threads[2], NULL, thread_C, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &threads[3], NULL, thread_D, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &threads[4], NULL, thread_E, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
	for(j=0; j < NTHREADS; j++){

	      pthread_join( threads[j], NULL);
	}
     exit(EXIT_SUCCESS);
}