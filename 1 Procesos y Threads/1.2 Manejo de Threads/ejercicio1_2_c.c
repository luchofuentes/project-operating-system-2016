#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define NTHREADS 5

sem_t semaforos[NTHREADS];
sem_t ab,ac;


void * thread_A (void * nro){
	while(1){
		sem_wait(&semaforos[0]);
		printf("A");
		if(sem_trywait(&ab)==0){;
			sem_post(&semaforos[0]);
			sem_post(&ac);
		}
		else{
			if(sem_trywait(&ac)==0){
				sem_post(&semaforos[2]);
			}
			else{
				sem_post(&semaforos[1]);
				sem_post(&ab);
			}
		}
	}
}

void * thread_B (void * nro){
	while(1){
		sem_wait(&semaforos[1]);
		printf("B");
		sem_post(&semaforos[2]);
	}
}

void * thread_C (void * nro){
	while(1){
		sem_wait(&semaforos[2]);
		printf("C");
		sem_post(&semaforos[3]);
	}
}

void * thread_D (void * nro){
	while(1){
		sem_wait(&semaforos[3]);
		printf("D");
		sem_post(&semaforos[4]);
	}
}

void * thread_E (void * nro){
	while(1){
		sem_wait(&semaforos[4]);
		printf("E");
		sem_post(&semaforos[0]);
	}
}


int main(){

	pthread_t threads [NTHREADS];
	int i,j,ret;
	int nros [NTHREADS] = {0,1,2,3,4};
	
	for(int i=0; i<NTHREADS; i++){
		sem_init(&semaforos[i],0,0);
	}
	sem_init(&ab,0,0);
	sem_init(&ac,0,0);
	
	sem_post(&semaforos[0]);
	sem_post(&ab);

 	ret = pthread_create( &threads[0], NULL, thread_A, NULL);
   	if(ret)
   	{
       	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
   	}

	ret = pthread_create( &threads[1], NULL, thread_B, NULL);
   	if(ret)
   	{
       	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
   	}

   	ret = pthread_create( &threads[2], NULL, thread_C, NULL);
   	if(ret)
   	{
       	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
   	}
   	ret = pthread_create( &threads[3], NULL, thread_D, NULL);
   	if(ret)
   	{
       	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
   	}
   	ret = pthread_create( &threads[4], NULL, thread_E, NULL);
   	if(ret)
   	{
       	printf("Error - pthread_create() return code: %d\n",ret);
       	exit(EXIT_FAILURE);
   	}

	for(j=0; j < NTHREADS; j++){

	      pthread_join( threads[j], NULL);
	}
     exit(EXIT_SUCCESS);
}