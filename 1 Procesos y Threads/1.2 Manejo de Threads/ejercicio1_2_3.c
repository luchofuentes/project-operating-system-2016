#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

sem_t semI,semp1,semp2,semp3,semp4,semp5,semp6;

void * nodoI(){
	while(1){
		sem_wait(&semI);
		printf("I-> ");
		sem_post(&semp1);
	}
	
}
void * nodop1(){
	while(1){
		sem_wait(&semp1);
		printf("1-> ");
		sem_post(&semp2);
		sem_post(&semp3);
		sem_post(&semp4);
	}
}
void * nodop2(){
	while(1){
		sem_wait(&semp2);
		printf("2-> ");
		sem_post(&semp6);
	}
}
void * nodop3(){
	while(1){
		sem_wait(&semp3);
		sem_wait(&semp3);
		sem_wait(&semp3);
		printf("3-> ");
		sem_post(&semp6);
	}
}
void * nodop4(){
	while(1){
		sem_wait(&semp4);
		printf("4-> ");
		sem_post(&semp3);
		sem_post(&semp5);
	}
}
void * nodop5(){
	while(1){
		sem_wait(&semp5);
		printf("5-> ");
		sem_post(&semp3);
		sem_post(&semp6);
	}
}
void * nodop6(){
	while(1){
		sem_wait(&semp6);
		sem_wait(&semp6);
		sem_wait(&semp6);
		printf("6-> ");
		sem_post(&semI);
	}
}

int main(){

	pthread_t tI,tp1,tp2,tp3,tp4,tp5,tp6;
	int j,ret;
	sem_init(&semI,0,1);
	sem_init(&semp1,0,0);
	sem_init(&semp2,0,0);
	sem_init(&semp3,0,0);
	sem_init(&semp4,0,0);
	sem_init(&semp5,0,0);
	sem_init(&semp6,0,0);
	
	//bloque de creacion de Threads
	ret = pthread_create( &tI, NULL, nodoI, NULL );
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &tp1, NULL, nodop1,NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &tp2, NULL, nodop2, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &tp3, NULL, nodop3, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &tp4, NULL, nodop4, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
	ret = pthread_create( &tp5, NULL, nodop5, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
	ret = pthread_create( &tp6, NULL, nodop6, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
	pthread_join( tI, NULL);
	pthread_join( tp1, NULL);
	pthread_join( tp2, NULL);
	pthread_join( tp3, NULL);
	pthread_join( tp4, NULL);
	pthread_join( tp5, NULL);
	pthread_join( tp6, NULL);
     exit(EXIT_SUCCESS);
}