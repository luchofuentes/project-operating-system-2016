#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>

int n,i,j,k;

static void notest(int **matriz1, int ** matriz2){

	int nro=0;
	for(i=0; i<n; i++){
		for(j=0; j<n; j++){
			printf("Introduzca un entero: ");
			scanf("%d", &nro);
			matriz1[i][j]=nro;
		}
	}

	printf("\n\nMatriz 1 : \n");
	for(i=0; i<n; i++){
        printf("[");
		for(j=0; j<n; j++){
			printf("%d\t", matriz1[i][j]);
		}
		printf("]\n");
	}

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("Introduzca un entero: ");
            scanf("%d",&nro);
            matriz2[i][j]=nro;
        }
    }

    printf("\n\nMatriz 2 : \n");
 	for(i=0; i<n; i++){
        printf("[");
		for(j=0; j<n; j++){
			printf("%d\t", matriz2[i][j]);
		}
		printf("]\n");
	}

	printf("\n\n");  
}

static void test(int ** matriz1, int ** matriz2){

	for(i=0; i<n; i++)
		for(j=0; j<n; j++)
			matriz1[i][j]=rand()%10;  // Random entre 0 y 100
	
	printf("\n\nMatriz 1 : \n");
	for(i=0; i<n; i++){
        printf("[");
		for(j=0; j<n; j++){
			printf("%d\t", matriz1[i][j]);
		}
		printf("]\n");
	}

    for(i=0;i<n;i++)
        for(j=0;j<n;j++)
            matriz2[i][j] = rand()%10; 

    printf("\n\nMatriz 2 : \n");
 	for(i=0; i<n; i++){
        printf("[");
		for(j=0; j<n; j++){
			printf("%d\t", matriz2[i][j]);
		}
		printf("]\n");
	}   

	printf("\n\n");
}

int main(int argc, char ** argv){

	char fn[30],line[100];
	pid_t pid;
	FILE * file;
	int ** matriz1, **matriz2, suma=0, status=0;;

	while(n<4 || n>6){
		printf("Introduzca el tamaño de las matrices : ");
		scanf("%d",&n);
		if(n<4 || n>6){
			printf("El numero debe estar entre 4 y 6 \n");
		}
	}
	
	matriz1 = (int**)  malloc (sizeof (int*)*n);
	matriz2 = (int**)  malloc (sizeof (int*)*n);

	for(i=0;i<n;i++){
	    
	    matriz1[i] = (int*) malloc (sizeof (int) *n);
		matriz2[i] = (int*) malloc (sizeof (int) *n);
	}
	
	if(argc>1){
		if(strcmp(argv[1],"--test")==0)
			test(matriz1,matriz2);
		else if(strcmp(argv[1],"--notest")==0)
			notest(matriz1,matriz2);
		else
			test(matriz1,matriz2);
	}
	else if(argc==1){
		test(matriz1,matriz2);
	}

	printf("PID PADRE: %d \n", getpid());

	for(i=0; i<n; i++){

		pid=fork();
			
		if(pid==0){
			
			sprintf(fn,"file[%i].txt",i);
			file = fopen(fn,"w+"); //Abro/Creo un archivo i donde se almacenara la fila i

			if(file==NULL){
				printf("File Error\n");
				exit(1);
			}
			fprintf(file, "[");
			
			for(j=0; j<n; j++){
				suma=0;
				for(k=0;k<n;k++){

					suma+=matriz1[i][k]*matriz2[k][j];	
			    }

			    fprintf(file, "%d\t", suma);
			}

			fprintf(file, "]");
			fclose(file); //Se cierra el archivo i.
			printf("PPID: %d  PID:  %d  Fila Calculada:  %d\n", getppid(),getpid(),i);	
			exit(0);
		
		}
	}
	//int wpid;
	while (wait(&status)>0){
		//printf("esperando... %d \n",wpid);
	}

	// Muestro la matriz resultante
	printf("\n\nMatriz Resultante: \n");
	for(i=0;i<n;i++){

		sprintf(fn,"file[%i].txt",i);
		file = fopen(fn,"r");
		fgets(line,100,file);
		printf("%s\n",line);
		fclose(file); 
	    free(matriz1[i]);
		free(matriz2[i]);
	}

	free(matriz1);
	free(matriz2);

	return 0;
}