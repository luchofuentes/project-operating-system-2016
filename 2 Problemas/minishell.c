#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h> 
#include <dirent.h>

#define MAXIMO_ARGUMENTOS 20
#define BUFFERSIZE 1024

char directorioactual[BUFFERSIZE];

int comando_cd(char **arg);
int comando_cp(char **arg);
int comando_ls(char **arg);
int comando_echo(char **arg);
int comando_pwd(char **arg);
int comando_exit(char **arg);
int mshell_ejecutar(char **arg);
void mshell_parse_free(char **arg);
char ** mshell_parse(char * comando);

int (*comando_func[]) (char **) = {
  &comando_cd,
  &comando_cp,
  &comando_ls,
  &comando_echo,
  &comando_pwd,
  &comando_exit
}; 

char *comandos[] = {
  "cd",
  "cp",
  "ls",
  "echo",
  "pwd",
  "exit"
};

int main(int argc, char **argv){

    int status = 1;

    if (getcwd(directorioactual, sizeof(directorioactual)) == NULL)
        fprintf(stderr,"Error al obtener el directorio actual\n");

    while(status){
        
        char * IN = malloc(sizeof(char)*BUFFERSIZE);

        if (!IN) {
            fprintf(stderr,"Error con la memoria dinamica\n");
            exit(EXIT_FAILURE);
        }

        printf("[%s]$ ",directorioactual);
        fgets(IN,1024,stdin);

        status = mshell_ejecutar(mshell_parse(IN));

        free(IN);
    }

    return 0;
}

int comando_exit(char **arg){

    mshell_parse_free(arg);
    return 0;
}

int comando_cd(char **arg){

    /* El comando cd por el momento solamente funciona de la siguiente manera:
     * cd /path/to/change/
     */

    if(arg[1][0]=='\n'){
        fprintf(stderr, "Comando cd\nError con los argumentos pasados\n");
        return 1;
    }

    if(chdir(arg[1])!=0)  //Cambio de directorio
        fprintf(stderr, "Comando cd\nError al cambiar de directorio\n");

    if (getcwd(directorioactual, sizeof(directorioactual)) == NULL) // Actualizo el directorio actual
        fprintf(stderr,"Comando cd\nError al obtener el directorio actual\n");

    mshell_parse_free(arg);
    return 1;
}

int comando_cp(char **arg){

    /* El comando cp por el momento solamente funciona de la siguiente manera:
     * cp archivoorigen /path/to/copy/
     */

    if(arg[1][0]=='\n' || arg[2][0]=='\n'){
        fprintf(stderr, "Comando cp\nError con los argumentos pasados\n");
        return 1;
    }

    int in_fd, out_fd, n_chars;
    char buf[1024], * destino;
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

    if( (in_fd=open(arg[1], O_RDONLY)) == -1 ){
        fprintf(stderr, "Comando cp\nError abriendo el archivo a copiar\n");
        exit(EXIT_FAILURE);
    }

    destino = arg[2];
    strcat(destino,arg[1]);
  
    if( (out_fd=creat(destino, mode)) == -1 ){
        fprintf(stderr,"Comando cp\nError creando el archivo a copiar\n");
        exit(EXIT_FAILURE);
    }
 
    while( (n_chars = read(in_fd, buf, 1024)) > 0 ){
    
        if( write(out_fd, buf, n_chars) != n_chars ){
            fprintf(stderr,"Comando cp\nError");
            exit(EXIT_FAILURE);
        }
 
        if( n_chars == -1 ){
            fprintf(stderr,"Comando cp\nError");
            exit(EXIT_FAILURE);
        }
    }
 
    if( close(in_fd) == -1 || close(out_fd) == -1 ){
        fprintf(stderr,"Comando cp\nError cerrando los archivos\n");
        exit(EXIT_FAILURE);
    }
 
    mshell_parse_free(arg);
    return 1;
}

int comando_ls(char **arg){

    DIR *dir;
    char cwd[1024];
    struct dirent *entry;
    struct stat mystat;

    if(arg[1][0]=='\n'){

        getcwd(cwd, sizeof(cwd));
        dir = opendir(cwd);
    }
    else{
        dir = opendir(arg[1]);
    }

    while((entry = readdir(dir)) != NULL)
    {
        stat(entry->d_name, &mystat);    
        printf("%10ld",mystat.st_size);
        printf("\t%s\n", entry->d_name);
    }

    closedir(dir);

    mshell_parse_free(arg);
    return 1;
}

int comando_echo(char **arg){

    int i=1;

    while(arg[i][0]!='\n'){
        printf("%s ",arg[i]);
        i++;
    }

    printf("\n");
    mshell_parse_free(arg);
    return 1;
}

int comando_pwd(char **arg){

    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
       printf("%s\n", cwd);

    mshell_parse_free(arg);
    return 1;
}

int mshell_ejecutar(char **arg){

    int i;

    if(arg[0]==NULL)
        return 1; //Comando vacio.

    for(i=0; i<6;i++){
        if(strcmp(arg[0],comandos[i])==0){
            return (*comando_func[i])(arg);
        }
    }

    mshell_parse_free(arg);
    printf("Comando no encontrado\n");
    return 1;

}

void mshell_parse_free(char **arg){

    int i=0;

    while(arg[i][0]!='\n'){
        //free(arg[i]);
        i++;
    }

    //free(arg[i]);
    free(arg);
}

char ** mshell_parse(char * comando){

    char ** arg = malloc(sizeof(char*)*MAXIMO_ARGUMENTOS);
    char * argv,*marg;
    char delim[] = " \t\r\n\a";
    int posa,i;

    if (!arg) {
        fprintf(stderr,"Error con la memoria dinamica\n");
        exit(EXIT_FAILURE);
    }

    posa = 0;
 
    for (argv = strtok(comando, delim); argv; argv = strtok(NULL, delim)){
        
        marg = malloc(sizeof(char)*1024);
        
        if(!marg){
                fprintf(stderr,"Error con la memoria dinamica\n");
                exit(EXIT_FAILURE);
        }

        i = 0;
        
        while(argv[i]!='\0'){
            marg[i] = argv[i];
            i++;
        }
        marg[i] = argv[i];
        
        arg[posa]=argv;
        posa++;
    }

    argv = malloc(sizeof(char));
    argv[0]='\n';
    arg[posa]=argv;
    return arg;
}