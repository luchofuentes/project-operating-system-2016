#include <windows.h>
#include <stdio.h>

HANDLE aviso,mutex,fuente; // Semaforos

DWORD WINAPI cocineros(void* data) {

	while(1){
		printf("COCINERO durmiendo\n");
		WaitForSingleObject(aviso,INFINITE);
		WaitForSingleObject(mutex,INFINITE);
			printf("COCINERO cocinando\n");
			if (!ReleaseSemaphore(fuente, 1, NULL)){
            	printf("Cocinero fuente,ReleaseSemaphore error: %d\n", (int)GetLastError());
            }
        if (!ReleaseSemaphore(mutex, 1, NULL)){
        	printf("Cocinero mutex,ReleaseSemaphore error: %d\n", (int)GetLastError());
        }

	}

  	return 0;
}

DWORD WINAPI comensales(void* data) {

	while(1){
		WaitForSingleObject(mutex,INFINITE);
		if(WaitForSingleObject(fuente,0)!=WAIT_OBJECT_0){
			printf("COMENSAL La fuente esta vacia\n");
			if (!ReleaseSemaphore(aviso, 1, NULL)){
            	printf("Comensal aviso, ReleaseSemaphore error: %d\n", (int)GetLastError());
            }
            if (!ReleaseSemaphore(mutex, 1, NULL)){
            	printf("Comensal mutex,ReleaseSemaphore error: %d\n", (int)GetLastError());
            }
            WaitForSingleObject(fuente,INFINITE);
			printf("COMENSAL Pusieron comida en la fuente\n");
		}
		else
			printf("COMENSAL Hay comida\n"); 
			if (!ReleaseSemaphore(mutex, 1, NULL)){
            	printf("Comensal mutex,ReleaseSemaphore error: %d\n", (int)GetLastError());
            }
	}
	return 0;
}

int main() {

	HANDLE cocinero[2],comensal; //Threads 

	aviso = CreateSemaphore(NULL, 0, 1, NULL); 
	if (aviso == NULL) {
        printf("CreateSemaphore error: %d\n", (int)GetLastError());
        return 1;
    }

	mutex = CreateSemaphore(NULL, 1, 100, NULL);
	if (mutex == NULL) {
        printf("CreateSemaphore error: %d\n", (int)GetLastError());
        return 1;
    }

	fuente = CreateSemaphore(NULL, 0, 100, NULL);  
	if (fuente == NULL) {
        printf("CreateSemaphore error: %d\n", (int)GetLastError());
        return 1;
    }

	cocinero[0] = CreateThread(NULL, 0, cocineros, NULL, 0, NULL);
	if (!cocinero[0]) {
		printf("Error\n");
	}

	cocinero[1] = CreateThread(NULL, 0, cocineros, NULL, 0, NULL);
	if (!cocinero[1]) {
		printf("Error\n");
	}

	comensal = CreateThread(NULL, 0, comensales, NULL, 0, NULL);
	if (!comensal) {
		printf("Error\n");
	}

	WaitForSingleObject(cocinero[0], INFINITE);
	WaitForSingleObject(cocinero[1], INFINITE);
	WaitForSingleObject(comensal, INFINITE);

	CloseHandle(cocinero[0]);
	CloseHandle(cocinero[1]);
	CloseHandle(comensal);
	
	return 0;
}
