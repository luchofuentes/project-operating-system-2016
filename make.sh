#!/bin/sh

ejercicio=$1
archivo=$2
PATHPROCESOS="1 Procesos y Threads/1.1 Creación de Procesos/"
PATHHILOS="1 Procesos y Threads/1.2 Manejo de Threads/"
PATHPROBLEMAS="2 Problemas/"


echo "###############################################"
echo "# Primer Proyecto de Sistemas Operativos 2016 #"
echo "# Juan Ignacio Cangelosi - 107573             #"
echo "# Luciano Fuentes - 107570                    #"
echo "###############################################"
echo -e "\nCreación de Procesos:"
echo -e "\t1. Ejercicio 1"
echo -e "\nManejo de Threads - Utilizando la librería Posix Pthread:"
echo -e "\t2. Ejercicio 1"
echo -e "\t3. Ejercicio 2 a)"
echo -e "\t4. Ejercicio 2 b)"
echo -e "\t5. Ejercicio 2 c)"
echo -e "\t6. Ejercicio 2 d)"
echo -e "\t7. Ejercicio 3"
echo -e "\nProblemas"
echo -e "\t8. Exquisitez (Linux)"
echo -e "\t9. Mini Shell\n"
read -p "Seleccion el ejercicio a compilar (El ejecutable se llamara ejercicio) :" opcion

case "$opcion" in

	1)
		ARCHIVOACOMPILAR="${PATHPROCESOS}ejercicio1_1.c"
		;;
	2)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_1.c"
		;;
	3)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_a.c"
		;;
	4)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_b.c"
		;;
	5)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_c.c"
		;;
	6)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_d.c"
		;;
	7)
		ARCHIVOACOMPILAR="${PATHHILOS}ejercicio1_2_e.c"
		;;
	8)
		ARCHIVOACOMPILAR="${PATHPROBLEMAS}exquisitez_linux.c"
		;;
	9)
		ARCHIVOACOMPILAR="${PATHPROBLEMAS}minishell.c"
		;;
	*) 
		echo "Opcion Invalida"
		exit 1
   		;;

esac

echo "Compilando "${ARCHIVOACOMPILAR}
gcc -lpthread "${ARCHIVOACOMPILAR}" -o ejercicio
if [ $? -eq 0 ]; then
	echo "Compilacion exitosa."
    else 
        echo "Error en la Compilacion"
fi

