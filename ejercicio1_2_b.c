#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#define NTHREADS 5

sem_t semaforos[4];
char letras [NTHREADS] = {'C','D','E'};

void * thread_CDE (void * nro){
	while(1){
		sem_wait(&semaforos[*(int*) nro %4]);
		printf("%c", letras[*(int*)nro -1]);
		sem_post(&semaforos[(*(int*)nro+1) %4]);
	}
}
void * thread_A(){
	while(1){
		sem_wait(&semaforos[0]);
		printf("A");
		sem_post(&semaforos[1]);
	}
}
void * thread_B(){
	while(1){
		sem_wait(&semaforos[0]);
		printf("B");
		sem_post(&semaforos[1]);
	}
}

int main(){

	pthread_t threads [NTHREADS];
	int i,j,ret;
	int nros [NTHREADS] = {1,2,3};
	
	for(int i=0; i<4; i++){
		sem_init(&semaforos[i],0,0);
	}

	sem_post(&semaforos[0]);
	
	ret = pthread_create( &threads[0], NULL, thread_A, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	ret = pthread_create( &threads[1], NULL, thread_B, NULL);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}

	for(int i=0;i<3;i++){

	 	ret = pthread_create( &threads[i+2], NULL, thread_CDE, (void*) &nros[i]);
     	if(ret)
     	{
        	printf("Error - pthread_create() return code: %d\n",ret);
         	exit(EXIT_FAILURE);
    	}
    }

	for(j=0; j < NTHREADS; j++){

	      pthread_join( threads[j], NULL);
	}
     exit(EXIT_SUCCESS);
}